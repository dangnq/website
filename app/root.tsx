import type { LinksFunction } from "@remix-run/cloudflare";
import { cssBundleHref } from "@remix-run/css-bundle";
import {
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useNavigation,
} from "@remix-run/react";

import { ColorSchemeScript, Container, MantineProvider } from '@mantine/core';

//Commonly shared components
import '@mantine/core/styles/global.css';
import '@mantine/core/styles/TypographyStylesProvider.css';
import '@mantine/core/styles/CloseButton.css';
import '@mantine/core/styles/UnstyledButton.css';
import '@mantine/core/styles/ScrollArea.css';
import '@mantine/core/styles/Combobox.css';
import '@mantine/core/styles/Input.css';
import '@mantine/core/styles/Popover.css';
//Actual components used
import '@mantine/core/styles/Button.css';
import '@mantine/core/styles/Container.css';
import '@mantine/core/styles/Divider.css';
import '@mantine/core/styles/Image.css';
import '@mantine/core/styles/Paper.css';
import '@mantine/core/styles/Card.css'; //Card is built on Paper
import '@mantine/core/styles/SimpleGrid.css';
import '@mantine/core/styles/Tabs.css';
import '@mantine/core/styles/Timeline.css';
import '@mantine/core/styles/Text.css';
import '@mantine/core/styles/Title.css';

//Mantine reduce bloat -- check if import is correct
//import '@mantine/core/styles.css';

//Custom css
import '@fontsource-variable/source-code-pro';
import '@fontsource-variable/oswald';
import '@fontsource-variable/geologica';
import 'katex/dist/katex.min.css';
import "./styles/stylesheet.css";

//Custom Navbar component
import Navbar from "./components/navbar";
import Ukraine from "./components/ukraine";
import TopBarProgress from "react-topbar-progress-indicator";

export const links: LinksFunction = () => [
  ...(cssBundleHref ? [{ rel: "stylesheet", href: cssBundleHref }] : []),
];

export function Layout({ children }: { children: React.ReactNode }) {
  const navigation = useNavigation();
  TopBarProgress.config({
    barColors: {
      "0": "#66d9e8",
      "1.0": "#0b7285",
    },
    shadowBlur: 5
  });
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1"
        />
        <Meta />
        <Links />
        <ColorSchemeScript
          defaultColorScheme="dark"
        />
      </head>
      <body>
        <MantineProvider defaultColorScheme="dark" theme={{
          fontFamily: 'Geologica Variable, sans-serif;',
          headings: {
            fontFamily: 'Oswald Variable, sans-serif',
          },
          fontFamilyMonospace: 'Source Code Pro Variable, monospace',
        }}>
          {navigation.state !== "idle" ? <TopBarProgress /> : null}
          <Ukraine />
          <Container size="sm">
            <Navbar />
            {children}
          </Container>
        </MantineProvider>
        <ScrollRestoration />
        <Scripts />
      </body>
    </html>
  );
}

export default function App() {
  return <Outlet />;
}
