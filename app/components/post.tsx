import { Title, Text } from "@mantine/core";
import { Link } from "@remix-run/react";

import '~/styles/post.css'

import type { PostMeta } from "~/.server/posts";
export const Post = ({ slug, frontmatter }: PostMeta) => {
  return (
    <>
      <article>
        <Link to={`/blog/${slug}`}>
          <Title order={2}>{frontmatter.title}</Title>
        </Link>
        <time
          dateTime={frontmatter.published}
        >
          {frontmatter.published.replace(/-/g, "/")}
        </time>
        <Text size="lg">{frontmatter.description}</Text>
      </article>
    </>
  );
};
