import { Card, Text, Image, Group, Title, Space, Button } from "@mantine/core";

export type LastFMUserGetRecentTracksResponse = {
  recenttracks: {
    track: Array<{
      artist: {
        mbid: string;
        '#text': string;
      };
      streamable: 0 | 1;
      image: Array<{
        '#text': string;
        size: string;
      }>;
      mbid: string;
      album: {
        mbid: string;
        '#text': string;
      };
      name: string;
      url: string;
      date: {
        uts: string;
        '#text': string;
      };
      '@attr'?: {
        nowplaying: "true"
      };
    }>;
    '@attr': {
      user: string;
      totalPages: string;
      page: string;
      perPage: string;
      total: string;
    };
  };
};

export function LastFmUserRecentTrack({ data, }: { data: LastFMUserGetRecentTracksResponse }) {

  if (data.recenttracks == null) {
    return <Text>I&apos;m not listening to anything right now &#129393;</Text>
  } else {
    const track = data.recenttracks.track[0];
    return <Card shadow="sm" padding="lg" radius="md" withBorder
      style={{ display: "inline-block", marginTop: "1rem" }}>
      {track.image[2]["#text"]
        ? <><Card.Section>
          <Image src={data.recenttracks.track[0].image[2]["#text"]} width={150}
            alt="Album cover" />
        </Card.Section>
          <Space h="sm" /></>
        : <></>}
      <Text fw={700}>&#127925; {data.recenttracks.track[0].name}</Text>
      <Text>&#128483; {data.recenttracks.track[0].artist["#text"]}</Text>
      <Space h="sm" />
      <a href={data.recenttracks.track[0].url} rel="noopener noreferrer"
        className="nohover">
        <Button color="red.8" fullWidth >Open on Last.FM</Button></a>
    </Card>
  }
}

export async function LastFmGetUserRecentTrack(user_name: string, api_key: string): Promise<LastFMUserGetRecentTracksResponse> {
  const data: Promise<LastFMUserGetRecentTracksResponse> =
    fetch(`https://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks
      &user=${user_name}&api_key=${api_key}&limit=1&nowplaying=true&format=json`)
      .then(response => response.json());
  return data;
}

