export default function Ukraine() {
    return (
        <a
            href="https://u24.gov.ua"
            target="_blank"
            rel="noopener noreferrer"
            title="Donate to support freedom."
            style={{
                background: '#000',
                display: 'flex',
                justifyContent: 'center',
                padding: '5px',
                zIndex: 10000,
                textDecoration: 'none',
            }}
        >
            <div role="img" aria-label="Flag of Ukraine" style={{ height: '25px', marginRight: '10px' }}>
                <div style={{ width: '40px', height: '12.5px', background: '#005BBB' }}></div>
                <div style={{ width: '40px', height: '12.5px', background: '#FFD500' }}></div>
            </div>
            <div style={{ color: 'white', lineHeight: '25px' }}>
                Donate to support freedom.
            </div>
        </a>
    );
}