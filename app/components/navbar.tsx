import { NavLink } from "@remix-run/react";
import { Divider, List, Title } from "@mantine/core";
import Typewriter from "typewriter-effect";

import "~/styles/navbar.css";

export default function Navbar() {
  const typeSpeed = 50;

  return (
    <>
      <nav>
        <Title order={2} mt="1rem">
          <Typewriter onInit={(typewriter) => {
            typewriter
              .changeDeleteSpeed(75)
              .typeString('Nguyee')
              .pauseFor(typeSpeed)
              .deleteChars(2)
              .typeString('êx')
              .pauseFor(typeSpeed)
              .deleteChars(2)
              .typeString('ễn ')
              .typeString('Quang ')
              .typeString('Dd')
              .pauseFor(typeSpeed)
              .deleteChars(2)
              .typeString('Đaw')
              .pauseFor(typeSpeed)
              .deleteChars(2)
              .typeString('ăng')
              .start()
          }}></Typewriter>
        </Title>
        <List>
          <List.Item><NavLink to="/" rel="prefetch">Home</NavLink></List.Item>
          <List.Item><NavLink to="/blog" rel="prefetch">Blog</NavLink></List.Item>
          <List.Item><NavLink to="/tidbits" rel="prefetch">Tidbits</NavLink></List.Item>
          <List.Item><NavLink to="/photos" rel="prefetch">Photos</NavLink></List.Item>
          <List.Item><NavLink to="/resume" rel="prefetch">Résumé</NavLink></List.Item>
          <List.Item><NavLink to="/contacts" rel="prefetch">Contacts</NavLink></List.Item>        </List>
      </nav>
      <Divider mt="xs" mb="xs" size="md" />
    </>
  )
}
