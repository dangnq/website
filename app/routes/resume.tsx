import type { MetaFunction } from "@remix-run/cloudflare";
import { Timeline, Text } from "@mantine/core";
import PageTitle from "~/components/pageTitle";


export const meta: MetaFunction = () => {
  return [
    { title: "Resume" },
    { name: "description", content: "Dang Nguyen Quang's Resume" },
  ];
};

export default function Resume() {
  const resume = [
    {
      time: "2024-",
      description: "Student at FH Aachen Jülich Campus studying Electrical Engineering",
    },
    {
      time: "2024",
      description: "Two weeks of German course focusing on teamwork, data representation and writing"
    },
    {
      time: "2024",
      description: "One week Coding Dojo, with a focused on Python and Data Science."
    },
    {
      time: "2024",
      description: "One week internship in FH Aachen Jülich Metal workshop",
    },
    {
      time: "2023-",
      description: "German Arbitur year.",
    },
    {
      time: "2023",
      description: "Graduated with highest merit."
    },
    {
      time: "Summer 2021",
      description: "Random ahh internship (I forgor) for 2 months working on embedded software."
    },
    {
      time: "Summer 2020",
      description: "Barista for a prestigious specialty café"
    }];

  return (
    <>
      <PageTitle title="My résumé" />
      <Timeline radius="lg" active={-1} bulletSize={25}>
        {resume.map((year, index) => (
          <Timeline.Item title={year.time} key={index}>
            <Text c="dimmed">{year.description}</Text>
          </Timeline.Item>
        ))}
      </Timeline>
    </>
  );
}
