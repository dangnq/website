import { TypographyStylesProvider } from "@mantine/core"
import { Outlet } from "@remix-run/react"

export default function Blog() {
  return (
    <div style={{
      paddingBottom: "30%" // Makes it easier for reader
    }}>
      <TypographyStylesProvider><Outlet></Outlet></TypographyStylesProvider>
    </div>
  )
}
