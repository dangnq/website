import type { MetaFunction } from "@remix-run/cloudflare";
import { SimpleGrid, Space } from "@mantine/core";
import PageTitle from "~/components/pageTitle";
export const meta: MetaFunction = () => {
  return [
    { title: "Contacts" },
    { name: "description", content: "Dang Nguyen Quang's Personal contacts" },
  ];
};

export default function Contacts() {
  const contacts = [
    {
      site: "Instagram",
      link: "https://www.instagram.com/dem_tonez/"
    },
    {
      site: "Codeberg",
      link: "https://codeberg.org/dangnq"
    },
    {
      site: "Email",
      link: "mailto:dang.nq06@gmail.com"
    }
  ];
  return (
    <>
      <PageTitle title="How to contact me" />
      <SimpleGrid cols={3}>
        {contacts.map((site, i) => (
          <a href={site.link} rel="noreferrer noopener" key={i}>{site.site}</a>
        ))}
      </SimpleGrid>
      <Space h="md"/>
      <a href='https://strava.com/athletes/123147307' 
        style={{ "display": "inline-block", "backgroundColor": "#FC5200", "color": "#fff", "padding": "5px 10px 5px 30px", "fontSize": "11px", "fontFamily": "Helvetica, Arial, sans-serif", "whiteSpace": "nowrap", "textDecoration": "none", "backgroundRepeat": "no-repeat", "backgroundPosition": "10px center", "borderRadius": "3px", "backgroundImage": "url('https://badges.strava.com/logo-strava-echelon.png')"}} target="_clean">
        Follow me on
        <img src='https://badges.strava.com/logo-strava.png' alt='Strava' style={{ marginLeft: '2px', verticalAlign: 'text-bottom' }} height={13} width={51} />
      </a >
    </>
  );
}
