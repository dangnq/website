import { SimpleGrid, Paper } from "@mantine/core"
import { Post } from "~/components/post"
import { useLoaderData } from "@remix-run/react";
import getPosts from "~/.server/posts";
import { MetaFunction } from "@remix-run/cloudflare";
import PageTitle from "~/components/pageTitle";

export async function loader() {
  const posts = getPosts();
  return { posts };
}

export const meta: MetaFunction = () => {
  return [
    { title: "My blogs" },
    { name: "description", content: "Quang Dang Nguyen's blog lists" }
  ]
}


export default function BlogsList() {
  const { posts } = useLoaderData<typeof loader>();
  return (
    <>
      <PageTitle title="My blog posts" />
      <SimpleGrid style={{ paddingTop: "1rem" }}>
        {posts.map((post) => (
          <Paper key={post.slug} className="post" shadow="md" radius="md" withBorder p="xl">
            <Post {...post} />
          </Paper>
        ))}
      </SimpleGrid>
    </>
  )
}
