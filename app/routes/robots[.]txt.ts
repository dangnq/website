import { generateRobotsTxt } from 'remix-seo-plus'

export function loader() {
    return generateRobotsTxt([
        { type: "sitemap", value: "https://dangnq.com/sitemap.xml" },
    ]);
}
