import type { LoaderFunctionArgs } from "@remix-run/cloudflare";
import { generateSitemap } from "remix-seo-plus";

export async function loader({ request }: LoaderFunctionArgs) {
    // eslint-disable-next-line import/no-unresolved
    const build = await import("virtual:remix/server-build");
    return generateSitemap(request, build.routes, {
        siteUrl: "https://dangnq.com",
    });
}
