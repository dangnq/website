import type { LoaderFunctionArgs, MetaFunction } from "@remix-run/cloudflare";
import { Text, Title } from "@mantine/core";
import { Suspense } from "react";
import { Await, useLoaderData } from "@remix-run/react";

import { LastFmGetUserRecentTrack, LastFmUserRecentTrack } from "~/components/lastfm";
import PageTitle from "~/components/pageTitle";

export const meta: MetaFunction = () => {
  return [
    { title: "Tidbits" },
    { name: "description", content: "Dang Nguyen Quang's experimental tidbits dump" },
  ];
};

export async function loader({ context, }: LoaderFunctionArgs) {
  //@ts-expect-error Api keys from cloudflare can only be known from runtime
  const api_key = context.cloudflare.env.LASTFM_API;
  const user_name = "double-dongles";
  const data = LastFmGetUserRecentTrack(user_name, api_key)
  return { data };
}
export default function Tidbits() {
  const { data } = useLoaderData<typeof loader>();
  return (
    <>
      <PageTitle title="Random tidbits" />
      <Title order={2}>Current song playing</Title>
      <Suspense fallback={<Text>I&apos;m not listening to anything right now &#129393;</Text>}>
        <Await resolve={data}>
          {(data) => <LastFmUserRecentTrack {...{ data }} />}
        </Await>
      </Suspense>
    </>
  );
}

