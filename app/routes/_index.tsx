import type { MetaFunction } from "@remix-run/cloudflare";
import { Text, Title, Select, Space, Divider } from "@mantine/core";
import PageTitle from "~/components/pageTitle";

export const meta: MetaFunction = () => {
  return [
    { title: "Quang Dang Nguyen" },
    { name: "description", content: "Quang Dang Nguyen's website" },
  ];
};

export default function Index() {
  return (
    <>
      <PageTitle title="Home" />
      <Title order={5}>
        <Select
          data={["Chào mừng đến với", "Welcome to", "Wilkommen auf", "Bienvenue sur"]}
          allowDeselect={false}
          defaultValue="Welcome to"
          maw="13em"
          size="md"
          withCheckIcon={false}
          style={{ position: "relative", display: "inline-block" }}
          variant="unstyled" />
        Dang&apos;s Lair &#128075;
      </Title>
      <Title order={3}>Hello, my name is Dang!</Title>
      <section>
        <Text>I study <Text span c="pink.7">electrical engineering,</Text> I do <Text span c="grape.6">full stack web development,</Text>
          <Text span c="indigo.6"> and I play music</Text>.
        </Text>
        <Text>I cycle, drink coffee and obsess over languages.</Text>
        <Text>I speak Vietnamese, English, German, Portugese and French.</Text>
      </section>
      <section>
        <Text>
          I maintain <a href="https://codeberg.org/dangnq/dangnq.com" rel="noreferrer noopener"> this web page</a> and its <Text span c="dimmed">rather lackluster</Text> blog,
          as well as <a href="https://codeberg.org/dangnq/dotfiles" rel="noreferrer noopener">my Nix dotfiles</a>.
        </Text>
        <Text>
          I also have a Minecraft Creative server
          that is hosted on Contabo
        </Text>
        <Text>
          Go to <a href="https://sillycat.gay">mc.sillycat.gay</a> and
          say haii to us :3 (please say haii on email first tho)
        </Text>
      </section>
      <Space h="md" />
      <section>
        <Text c="dimmed">This website uses Cloudflare&apos;s <a href="https://www.cloudflare.com/web-analytics/" rel="noreferrer noopener">non-intrusive web tracking technology.</a></Text>
        <Text c="dimmed">Most of the content in this page should be <a href="/LICENSE.txt">open source and/or open access.</a></Text>
      </section>
    </>
  );
}
