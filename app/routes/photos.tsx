import { MetaFunction} from '@remix-run/cloudflare';
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry"
import Imgix from "react-imgix";
import { useLoaderData, Await } from "@remix-run/react";
import { Suspense } from 'react';

import 'lazysizes';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';

import '~/styles/photos.css'
import PageTitle from '~/components/pageTitle';
import TopBarProgress from 'react-topbar-progress-indicator';

export const meta: MetaFunction = () => {
  return [
    { title: "Dang Nguyen's Photo" },
    { name: "description", content: "My photos" }
  ]
}

export async function loader() {
  // We fetch the photo list from the origin server
  const photos: Promise<Array<string>> = fetch("https://files.dangnq.com/photos/photos.json").then(photos => photos.json());
  return { photos };
}

export default function Photos() {
  // But we fetch the photos from the secondary cdn server for speed and processing
  const cdn = "https://cdn.dangnq.com/";
  const { photos } = useLoaderData<typeof loader>(); // Typescript
  return (
    <div>
      <PageTitle title="Photos" />
      <Suspense
        fallback=<TopBarProgress/>>
        <Await resolve={photos}>
          {(photos) =>
            <ResponsiveMasonry columnsCountBreakPoints={{ 1: 1, 767: 2, 1199: 3 }}>
              <Masonry gutter="0.25vw">
                {photos.map((name: string, id: number) =>
                  <Imgix key={id}
                    className="picture lazyload"
                    src={cdn.concat(name)}
                    attributeConfig={{
                      src: "data-src",
                      srcSet: "data-srcset",
                      sizes: "data-sizes",
                    }}
                    sizes="(min-width: 1200px) calc(20vw - 0.25vw - 8px), (min-width: 768px) calc(50vw - 0.25vw - 8px), calc(90vw -8px)"
                    htmlAttributes={{
                      src: cdn.concat(name, "?blur=200&w=100&auto=format"),
                      alt: name,
                    }}
                  />
                )}
              </Masonry>
            </ResponsiveMasonry>}
        </Await>
      </Suspense>
    </div>
  );
}
