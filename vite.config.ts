import {
  vitePlugin as remix,
  cloudflareDevProxyVitePlugin as remixCloudflareDevProxy,
} from "@remix-run/dev";
import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";

import mdx from "@mdx-js/rollup";
import remarkFrontmatter from "remark-frontmatter";
import remarkMdxFrontmatter from "remark-mdx-frontmatter";
import { default as rehypePrettyCode } from "rehype-pretty-code";
import remarkGfm from "remark-gfm";
import rehypeExternalLinks from "rehype-external-links";
import remarkMath from "remark-math";
import rehypeKatex from "rehype-katex";
import { nodePolyfills } from 'vite-plugin-node-polyfills'
import rehypeAutolinkHeadings from "rehype-autolink-headings";
import rehypeSlug from "rehype-slug";
import remarkToc from "remark-toc";

// Single fetch migration fix
declare module "@remix-run/cloudflare" {
  // or cloudflare, deno, etc.
  interface Future {
    v3_singleFetch: true;
  }
}


export default defineConfig({
  plugins: [remixCloudflareDevProxy(), mdx({
    remarkPlugins: [remarkFrontmatter,
      remarkMdxFrontmatter,
      remarkGfm, remarkMath, remarkToc],
    rehypePlugins: [[rehypePrettyCode, { theme: "one-dark-pro" }],
    [rehypeExternalLinks, { rel: ['noopener', 'noreferrer'] }], rehypeKatex, rehypeSlug,
      rehypeAutolinkHeadings],
  }), remix({
    future: {
      v3_singleFetch: true,
      v3_fetcherPersist: true,
      v3_relativeSplatPath: true,
      v3_throwAbortReason: true,
      v3_lazyRouteDiscovery: true,
      v3_routeConfig: true,
    }
  }), nodePolyfills(),
  tsconfigPaths()],
});
