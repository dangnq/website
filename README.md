# My own personal website

This website is built using RemixJS v2 + Vite + Cloudflare Pages

# On NixOS

Due to a peculiar way of linking workerd of Cloudflare pages, eg. they don't static link it,
making it impossible to run on NixOS. That's why you'll need to setup 
[nix-ld](https://github.com/Mic92/nix-ld) in order to change the lookup path for the 
static linker.
